%% User Interface
% main_path = 'P:\CMaldonadoRodas\Cadaver Knee\Cadaver Downhill Simplex\Bardiya_Tracked_PSO\';
main_path = 'P:\iTWA_Instrumented_Total_Wrist\Subjects\CADAVER\WN00000\Autoscoper';

% boneName = 'Femur_Tracked';
boneName = 'FlexExt_mc3';

file_1 = fullfile(main_path,'NCCs',[boneName '_PSO.ncc']);
file_2 = fullfile(main_path,'NCCs',[boneName '.ncc']);

file_1_tra = fullfile(main_path,'Tracking',[boneName '_PSO.tra']);
file_2_tra = fullfile(main_path,'Tracking',[boneName '.tra']);

%% Read NCC File
fileID_1 = fopen(file_1,'r');
NCCs_all_1 = fscanf(fileID_1,'%f,%f,%f\n');
fclose(fileID_1);
NCCs_1 = reshape(NCCs_all_1,3,[])';
fileID_2 = fopen(file_2,'r');
NCCs_all_2 = fscanf(fileID_2,'%f,%f,%f\n');
fclose(fileID_2);
NCCs_2 = reshape(NCCs_all_2,3,[])';

% Plot
figure(1); clf
plot(NCCs_1(:,:),'DisplayName','NCCs','LineWidth',2); hold on
plot(NCCs_2(:,:),'.--','DisplayName','NCCs','LineWidth',2);
grid on;
legend('PSO - Camera 01','PSO - Camera 02','PSO - Final Cost Function','Camera 01','Camera 02','Final Cost Function')
title(['Normalized Cross-Correlation Values - ' boneName])
ax = gca;
ax.FontSize = 14;
ax.LineWidth = 2;
% xlim([0 102])

%% Read TRA File
bone_file1_16x1 = csvread(file_1_tra);
bone_file2_16x1 = csvread(file_2_tra);

counter = 1;
bone_file1_6x1 = nan(101,6);
bone_file2_6x1 = nan(101,6);
for iFrame = 1:600
    bone_file1_4x4{counter,1} = reshape(bone_file1_16x1(iFrame,1:16),[4 4])';
    bone_file2_4x4{counter,1} = reshape(bone_file2_16x1(iFrame,1:16),[4 4])';
    
    bone_file1_6x1(counter,1:3) = bone_file1_4x4{counter,1}(1:3,4)';
    bone_file2_6x1(counter,1:3) = bone_file2_4x4{counter,1}(1:3,4)';
    bone_file1_6x1(counter,4:6) = rad2deg(tform2eul(bone_file1_4x4{counter,1}));
    bone_file2_6x1(counter,4:6) = rad2deg(tform2eul(bone_file2_4x4{counter,1}));
    counter = counter + 1;
end

figure(2); clf
plot(bone_file1_6x1,'DisplayName','NCCs','LineWidth',2); hold on
plot(bone_file2_6x1,'.--','DisplayName','NCCs','LineWidth',2);
grid on;
% legend('PSO - Camera 01','PSO - Camera 02','PSO - Final Cost Function','Camera 01','Camera 02','Final Cost Function')
title(['Kinematics - ' boneName])
ax = gca;
ax.FontSize = 14;
ax.LineWidth = 2;
% xlim([0 102])

figure(3);
plot(bone_file1_6x1-bone_file2_6x1,'LineWidth',2)
title(['Kinematics Differences - ' boneName])
legend('Tx','Ty','Tz','Rot_x','Rot_y','Rot_z')
grid on;
ax = gca;
ax.FontSize = 14;
ax.LineWidth = 2;
% xlim([0 102])















