clear;
clc; fclose all;

% For every subject/task/model:
Subject_Name  = 'WN00000'; % I use the subject name to find the main path for autoscoper files
Task_Name     = 'FlexExt'; % I use the task name to find the .cfg file
Model_Name    = 'mc3_mc4'; % I use this for tracking file name

%% Needed Paths - IMPORTANT
subj_path = fullfile('C:\*********', Subject_Name);
trial_name = fullfile(subj_path,[Task_Name '.cfg']));
tracking_filename = fullfile(subj_path,[Task_Name '_' Model_Name '.tra']);
cam1_filter = fullfile(subj_path,'control_settings.vie');
cam2_filter = fullfile(subj_path,'control_settings.vie');
volume = 0; % Volume Handle


% Run Autoscoper - IMPORTANT - CHANGE THE PATH
exe_root_path = 'C:\Autoscoper-v2.1\build_new\install\bin\Release'; % Folder that has autoscoper.exe in it
system(fullfile(exe_root_path,'autoscoper.exe &'));

pause(1);

pause(3);
%% Load Data in Autoscoper
% open Connection
autoscoper_socket = openConnection('127.0.0.1');


%load Trial
loadTrial(autoscoper_socket,trial_name);

%Load tracking
loadTrackingData(autoscoper_socket, volume,tracking_filename);

pause(1);
%set Background (optional)
setBackground(autoscoper_socket,0.12);

%load filter settings (optional)
loadFilters(autoscoper_socket,0,cam1_filter);
loadFilters(autoscoper_socket,1,cam2_filter);

return;














