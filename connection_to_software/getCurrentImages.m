
autoscoper_socket = openConnection('127.0.0.1');

first_frame = 0;
last_frame = 100;

%%
ncc_values = [];
nextPose = [];
frame = 62;
volume = 0;
    
%set frame
setFrame(autoscoper_socket,frame);

%get pose for the frame
curPose = getPose(autoscoper_socket,volume,frame);

%%
cameraID = 0;
curPose = getPose(autoscoper_socket,volume,frame);
[A] = readImages(autoscoper_socket,volume,cameraID,curPose);

% figure(2); clf; hold on
% imshow(A)

figure(1); clf; hold on
subplot(2,3,1);
imshow(A(:,:,1))
title('Radiograph')
subplot(2,3,2);
imshow(A(:,:,2))
title('DRR')
subplot(2,3,3);
imshow(A(:,:,3))
title('Mask')

cameraID = 1;
[A] = readImages(autoscoper_socket,volume,cameraID,curPose);
subplot(2,3,4);
imshow(A(:,:,1))
subplot(2,3,5);
imshow(A(:,:,2))
subplot(2,3,6);
imshow(A(:,:,3))

Dist = sum(sqrt(sum((A(:,:,1) - A(:,:,2)) .^ 2))); 
